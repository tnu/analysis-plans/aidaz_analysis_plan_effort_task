This repository contains the analysis plan for the analysis of the effort task within the AIDA Zuerich study.

The analysis plan describes the analysis implemented and subsequently written up in the manuscript “Computational mechanisms of effort and reward decisions in depression and their relationship to relapse after antidepressant discontinuation”, which has been submitted for publication.

The PDF AnalysisPlanEffortTask_cleanVersionForPublication.pdf contains the final version of the analysis plan used in the publication.

Authors: Isabel M. Berwian, Julia G. Wenzel, Anne G.E. Collins, Erich Seifritz, Klaas E. Stephan, Henrik Walter and Quentin J. M. Huys 
